<?php

namespace Tests\GitContributorsBundle\Fetcher;

use GitContributorsBundle\Decorator\FetcherDecorator;
use GitContributorsBundle\Entity\Query;
use GitContributorsBundle\Fetcher\ContributorsFetcherInterface;
use GitContributorsBundle\Form\QueryType;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\TypeTestCase;

class FetcherDecoratorTest extends TypeTestCase
{
    /**
     * @var ContributorsFetcherInterface|ObjectProphecy
     */
    private $fetcher;

    /**
     * @var FetcherDecorator
     */
    private $decorator;

    /**
     * @var \stdClass
     */
    private $row1;

    /**
     * @var \stdClass
     */
    private $row2;

    /**
     * @var \stdClass
     */
    private $row3;

    public function setUp()
    {
        parent::setUp();

        $this->row1 = new \stdClass();
        $this->row1->name = 'B name';
        $this->row1->avatarUrl = 'https://avatar1.url';
        $this->row1->contributions = 15;

        $this->row2 = new \stdClass();
        $this->row2->name = 'C name';
        $this->row2->avatarUrl = 'https://avatar2.url';
        $this->row2->contributions = 100;

        $this->row3 = new \stdClass();
        $this->row3->name = 'A name';
        $this->row3->avatarUrl = 'https://avatar3.url';
        $this->row3->contributions = 3;

        $this->fetcher = $this->prophesize(ContributorsFetcherInterface::class);
        $this->fetcher->getContributorsFromApi('proper/repo')
            ->willReturn(['contributors' => [$this->row1, $this->row2, $this->row3]]);
        $this->decorator = new FetcherDecorator($this->fetcher->reveal());
    }

    public function testSortByContributionsDesc()
    {
        $form = $this->prepareForm('sortByContributionsDesc');
        $result = $this->decorator->getContributorsByForm($form);
        $expected = [
            'contributors' => [$this->row2, $this->row1, $this->row3],
        ];

        $this->assertEquals($expected, $result);
    }

    public function testSortByContributionsAsc()
    {
        $form = $this->prepareForm('sortByContributionsAsc');
        $result = $this->decorator->getContributorsByForm($form);
        $expected = [
            'contributors' => [$this->row3, $this->row1, $this->row2],
        ];

        $this->assertEquals($expected, $result);
    }

    public function testSortByNameDesc()
    {
        $form = $this->prepareForm('sortByNameDesc');
        $result = $this->decorator->getContributorsByForm($form);
        $expected = [
            'contributors' => [$this->row2, $this->row1, $this->row3],
        ];

        $this->assertEquals($expected, $result);
    }

    public function testSortByNameAsc()
    {
        $form = $this->prepareForm('sortByNameAsc');
        $result = $this->decorator->getContributorsByForm($form);
        $expected = [
            'contributors' => [$this->row3, $this->row1, $this->row2],
        ];

        $this->assertEquals($expected, $result);
    }

    private function prepareForm(string $sortType): Form
    {
        $formData = [
            'query' => 'proper/repo',
            'sortType' => $sortType,
        ];

        $form = $this->factory->create(QueryType::class);
        $form->submit($formData);

        return $form;
    }
}
