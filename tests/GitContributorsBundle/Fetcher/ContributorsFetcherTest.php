<?php

namespace Tests\GitContributorsBundle\Fetcher;

use GitContributorsBundle\Connector\ConnectorInterface;
use GitContributorsBundle\Fetcher\ContributorsFetcher;

class ContributorsFetcherTest extends \PHPUnit_Framework_TestCase
{
    public function testReturnedDataFormApiProperRepoName()
    {
        $connector = $this->prophesize(ConnectorInterface::class);
        $connector->getData('proper/name')->willReturn($this->getRawData());

        $fetcher = new ContributorsFetcher($connector->reveal());
        $result = $fetcher->getContributorsFromApi('proper/name');

        $this->assertEquals($this->getExpectedResult(), $result);
    }

    public function testReturnEmptyArrayWhenWrongRepoName()
    {
        $connector = $this->prophesize(ConnectorInterface::class);
        $connector->getData('wrong/name')->willThrow(new \Exception());

        $fetcher = new ContributorsFetcher($connector->reveal());
        $result = $fetcher->getContributorsFromApi('wrong/name');

        $expected = [
            'header' => [
                'repository' => 'wrong/name',
                'time' => time(),
            ],
            'contributors' => [],
        ];
        $this->assertEquals($expected, $result);
    }

    private function getRawData()
    {
        $row1 = new \stdClass();
        $row1->login = 'name1';
        $row1->avatar_url = 'https://avatar1.url';
        $row1->contributions = 15;
        $row1->unecessaryData = 'unecessary data 1';

        $row2 = new \stdClass();
        $row2->login = 'name2';
        $row2->avatar_url = 'https://avatar2.url';
        $row2->contributions = 10;
        $row2->unecessaryData = 'unecessary data 2';

        $row3 = new \stdClass();
        $row3->login = 'name3';
        $row3->avatar_url = 'https://avatar3.url';
        $row3->contributions = 3;
        $row3->unecessaryData = 'unecessary data 3';

        $rawData = [$row1, $row2, $row3];

        return json_encode($rawData);
    }

    private function getExpectedResult()
    {
        $row1 = new \stdClass();
        $row1->name = 'name1';
        $row1->avatarUrl = 'https://avatar1.url';
        $row1->contributions = 15;

        $row2 = new \stdClass();
        $row2->name = 'name2';
        $row2->avatarUrl = 'https://avatar2.url';
        $row2->contributions = 10;

        $row3 = new \stdClass();
        $row3->name = 'name3';
        $row3->avatarUrl = 'https://avatar3.url';
        $row3->contributions = 3;

        return [
            'header' => [
                'repository' => 'proper/name',
                'time' => time(),
            ],
            'contributors' => [$row1, $row2, $row3]
        ];
    }
}
