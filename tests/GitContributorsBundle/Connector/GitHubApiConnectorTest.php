<?php

namespace Tests\GitContributorsBundle\Connector;

use GitContributorsBundle\Connector\GitHubApiConnector;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\StreamInterface;

class GitHubApiConnectorTest extends \PHPUnit_Framework_TestCase
{
    public function testGetDataShouldReturnString()
    {
        $body = $this->prophesize(StreamInterface::class);
        $body->getContents()->willReturn('proper_response');

        $resources = $this->prophesize(Request::class);
        $resources->getBody()->willReturn($body->reveal());

        $client = $this->prophesize(Client::class);
        $client->request('GET', 'https://api.github.com/repos/org/repo/contributors')->willReturn($resources->reveal());

        $connector = new GitHubApiConnector($client->reveal());
        $connector->setApiUrl('https://api.github.com/repos/%s/contributors');
        $response = $connector->getData('org/repo');

        $this->assertEquals('proper_response', $response);
    }
}
