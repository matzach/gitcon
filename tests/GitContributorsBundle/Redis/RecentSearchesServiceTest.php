<?php

namespace Tests\GitContributorsBundle\Redis;

use GitContributorsBundle\Redis\RecentSearchesService;

class RecentSearchesServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testSaveWithEmptyList()
    {
        $data = ['key' => 'value'];
        $serializedData = serialize($data);

        $client = $this->prophesize(\Predis\Client::class);
        $client->lpush('list_name', $serializedData)->shouldBeCalledTimes(1);
        $client->llen('list_name')->willReturn(0);
        $client->rpop('list_name')->shouldNotBeCalled();

        $recentSearchesService = new RecentSearchesService($client->reveal(), 'list_name', 3);

        $recentSearchesService->save($data);
    }
}
