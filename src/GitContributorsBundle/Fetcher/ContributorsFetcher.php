<?php

namespace GitContributorsBundle\Fetcher;

use GitContributorsBundle\Connector\ConnectorInterface;

class ContributorsFetcher implements ContributorsFetcherInterface
{
    /**
     * @var ConnectorInterface
     */
    private $connector;

    public function __construct(ConnectorInterface $connector)
    {
        $this->connector = $connector;
    }

    public function getContributorsFromApi(string $repository): array
    {
        try {
            $json = $this->connector->getData($repository);
            $result = json_decode($json);
        } catch(\Exception $exception) {
            $result = [];
        }

        return $this->prepareData($result, $repository);
    }

    private function prepareData(array $data, string $repository): array
    {
        $contributors = [];

        $result['header'] = $this->prepareHeader($repository);
        foreach ($data as $item) {
            $contributors[] = $this->prepareRow($item);
        }
        $result['contributors'] = $contributors;

        return $result;
    }

    private function prepareHeader(string $repository): array
    {
        $header = [
            'repository' => $repository,
            'time' => time(),
        ];

        return $header;
    }

    private function prepareRow(\stdClass $item): \stdClass
    {
        $row = new \stdClass();
        $row->name = $item->login;
        $row->avatarUrl = $item->avatar_url;
        $row->contributions = $item->contributions;

        return $row;
    }
}
