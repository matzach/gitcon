<?php
/**
 * Created by PhpStorm.
 * User: mateusz
 * Date: 04.10.17
 * Time: 20:47
 */

namespace GitContributorsBundle\Fetcher;


interface ContributorsFetcherInterface
{
    public function getContributorsFromApi(string $repository): array;
}
