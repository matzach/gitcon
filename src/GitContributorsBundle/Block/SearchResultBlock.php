<?php

namespace GitContributorsBundle\Block;

use GitContributorsBundle\Redis\RecentSearchesService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchResultBlock extends AbstractBlockService
{
    /**
     * @var RecentSearchesService
     */
    private $recentSearchesService;

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();
        $contributors = $settings['contributors'];

        return $this->renderResponse(
            $blockContext->getTemplate(),
            ['contributors' => $contributors],
            $response
        );
    }

    public function configureSettings(OptionsResolver $resolver)
    {
        parent::configureSettings($resolver);
        $resolver->setDefaults(
            [
                'contributors' => [],
                'template' => 'GitContributorsBundle:Recent:search.html.twig',
            ]
        );
    }

    public function setRecentSearchesService(RecentSearchesService $recentSearchesService): self
    {
        $this->recentSearchesService = $recentSearchesService;

        return $this;
    }
}
