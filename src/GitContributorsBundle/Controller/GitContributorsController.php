<?php

namespace GitContributorsBundle\Controller;

use GitContributorsBundle\Entity\Query;
use GitContributorsBundle\Form\QueryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GitContributorsController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = new Query();
        $form = $this->createForm(QueryType::class, $query);
        $contributors = [];

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fetcher = $this->get('gitcon.fetcher_decorator');
            $contributors = $fetcher->getContributorsByForm($form);

            $client = $this->get('gitcon.recent_searches_service');
            $client->save($contributors);
        }

        return $this->render('GitContributorsBundle:Home:index.html.twig', [
            'form' => $form->createView(),
            'contributors' => $contributors,
        ]);
    }

    public function recentAction()
    {
        $client = $this->get('gitcon.recent_searches_service');
        $list = $client->get();

        return $this->render('GitContributorsBundle:Recent:recent.html.twig', ['recentSearches' => $list]);
    }
}
