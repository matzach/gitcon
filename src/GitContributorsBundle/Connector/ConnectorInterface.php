<?php

namespace GitContributorsBundle\Connector;

interface ConnectorInterface
{
    public function getData(string $key): string;
}
