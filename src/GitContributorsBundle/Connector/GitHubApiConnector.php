<?php

namespace GitContributorsBundle\Connector;

use GuzzleHttp\Client;

class GitHubApiConnector implements ConnectorInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiUrl;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getData(string $key): string
    {
        $url = sprintf($this->apiUrl, $key);
        $resources = $this->client->request('GET', $url);
        $content = $resources->getBody()->getContents();

        return $content;
    }

    public function setApiUrl(string $apiUrl): self
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }


}
