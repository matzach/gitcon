<?php

namespace GitContributorsBundle\Redis;

class RecentSearchesService
{
    /**
     * @var \Predis\Client
     */
    private $client;

    /**
     * @var string
     */
    private $listName;

    /**
     * @var int
     */
    private $listLength;

    public function __construct(\Predis\Client $client, string $listName, int $listLength)
    {
        $this->client = $client;
        $this->listName = $listName;
        $this->listLength = $listLength;
    }
    
    public function save(array $searchResult)
    {
        $this->client->lpush($this->listName, serialize($searchResult));

        while($this->client->llen($this->listName) > $this->listLength) {
            $this->client->rpop($this->listName);
        }
    }

    public function get(): array
    {
        $recentSearches = $this->client->lrange($this->listName, 0, $this->listLength - 1);
        $result = $this->unserialize($recentSearches);

        return $result;
    }

    private function unserialize(array $items): array
    {
        $result = [];

        foreach ($items as $item) {
            $object = unserialize($item);
            if (!$object) {
                $object = [];
            }

            $result[] = $object;
        }

        return $result;
    }
}
