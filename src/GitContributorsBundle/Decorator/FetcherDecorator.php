<?php

namespace GitContributorsBundle\Decorator;

use GitContributorsBundle\Fetcher\ContributorsFetcherInterface;
use Symfony\Component\Form\Form;

class FetcherDecorator
{
    /**
     * @var ContributorsFetcherInterface
     */
    private $fetcher;

    public function __construct(ContributorsFetcherInterface $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function getContributorsByForm(Form $form)
    {
        $repository = $form['query']->getData();
        $sortType = $form['sortType']->getData();

        $contributors = $this->fetcher->getContributorsFromApi($repository);
        $contributors = $this->sort($contributors, $sortType);

        return $contributors;
    }

    private function sort(array $contributors, string $sortType = 'sortByContributionsAsc'): array
    {
        usort($contributors['contributors'], [$this, $sortType]);

        return $contributors;
    }

    private function sortByNameAsc(\stdClass $contributor1, \stdClass $contributor2): int
    {
        $name1 = strtolower($contributor1->name);
        $name2 = strtolower($contributor2->name);

        return $this->sortBy($name1, $name2);
    }

    private function sortByNameDesc(\stdClass $contributor1, \stdClass $contributor2): int
    {
        $name1 = strtolower($contributor1->name);
        $name2 = strtolower($contributor2->name);

        return $this->sortBy($name2, $name1);
    }

    private function sortByContributionsAsc(\stdClass $contributor1, \stdClass $contributor2): int
    {
        $contributions1 = strtolower($contributor1->contributions);
        $contributions2 = strtolower($contributor2->contributions);

        return $this->sortBy($contributions1, $contributions2);
    }

    private function sortByContributionsDesc(\stdClass $contributor1, \stdClass $contributor2): int
    {
        $contributions1 = strtolower($contributor1->contributions);
        $contributions2 = strtolower($contributor2->contributions);

        return $this->sortBy($contributions2, $contributions1);
    }

    /**
     * @param mixed $name1
     * @param mixed $name2
     * @return int
     */
    private function sortBy($name1, $name2): int
    {
        $result = ($name1 < $name2) ? -1 : 1;

        if ($name1 === $name2) {
            $result = 0;
        }

        return $result;
    }
}
