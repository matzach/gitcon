<?php

namespace GitContributorsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Query
{
    /**
     * @Assert\Length(
     *     min = 1,
     *     max = 100,
     *     minMessage="Search query can not be blank.",
     *     maxMessage="Search query can not be longer then {{ limit }}."
     * )
     *
     * @var string
     */
    private $query = '';

    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     * @return Query
     */
    public function setQuery($query): self
    {
        $this->query = $query;

        return $this;
    }
}
