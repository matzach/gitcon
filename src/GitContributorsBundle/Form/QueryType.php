<?php

namespace GitContributorsBundle\Form;

use GitContributorsBundle\Entity\Query;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QueryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query', TextType::class, [
                'translation_domain' => 'messages',
                'label' => 'form_repo_name'
            ])
            ->add('sortType', ChoiceType::class, [
                'translation_domain' => 'messages',
                'choices'  => [
                    'form_con_asc' => 'sortByContributionsAsc',
                    'form_con_desc' => 'sortByContributionsDesc',
                    'form_name_asc' => 'sortByNameAsc',
                    'form_name_desc' => 'sortByNameDesc',
                ],
                'mapped' => false,
            ])
            ->add('search', SubmitType::class, [
                'translation_domain' => 'messages',
                'label' => 'form_search'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Query::class,
        ));
    }
}
